module CommentRipper
	class UnmatchedTokenException < RuntimeError
		def initialize(token)
			@token = token
		end
		
		def to_s
			"Unmatched token: #{@token.symbol} at L#{@token.position.line} C#{@token.position.character}"
		end
	end
end
