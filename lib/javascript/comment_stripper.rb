require 'javascript/lexer'
require 'exceptions'

module CommentRipper
	class Javascript
		def initialize(opts={})
			# nothing here yet
		end

		def strip(tree_or_string)
			if(tree_or_string.is_a? String)
				return strip(JavascriptLexer.new(tree_or_string).lex)
			end
			tree = tree_or_string
			tree.map do |t|
				case t
				when SingleLineComment, MultiLineComment
					""
				else
					t.to_s
				end
			end.join
		end
	end
end
