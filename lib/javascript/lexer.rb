require 'tokens/token'
require 'tokens/words'
require 'tokens/whitespace'
require 'tokens/comments'
require 'tokens/quotes'
require 'tokens/special'

module CommentRipper
	class JavascriptLexer
		def initialize(string)
			@string = string
		end

		def lex
			ret = @string
			[:lex_1, :lex_2, :lex_3].each do |l|
				ret = send(l, ret)
			end
			ret
		end

		private
		def lex_1(string)
			i = 0
			tokens = []
			line = 1
			char = 1
			until string[i] == nil
				match = nil
				basic_lexer_order = [SingleLineCommentStart, PreservedCommentStart, MultiLineCommentStart, 
					MultiLineCommentEnd, Newline, Tab, Space, SingleQuote, DoubleQuote, Slash,
					EscapeCharacter]
					
				basic_lexer_order.each do |token|
					token_length = token.symbol.length
					str = string[i..(i+token_length-1)]
					if(token.match(str))
						match = token.new(line, char)
						i += token_length-1
						char += token_length-1
						break
					end
				end
				if(tokens.last.is_a?(EscapeCharacter) and (match.is_a? Comment or match.is_a? Newline))
					tokens.pop
					i -= (match.symbol.length-1)
					tokens << Character.new(line, char, string[i].chr)
				else
					if(match.is_a? Newline)
						line += 1
						char = 0
					end
					if !match.nil?
						tokens << match
					else
						tokens << Character.new(line, char, string[i].chr)
					end
				end
				i += 1
				char += 1
			end
			tokens
		end
		
		def lex_2(stream)
			tree = []
			until stream.empty?
				token = stream.shift
				if(token.is_a? Character)
					word = Word.new
					word << token
					while true
						if(stream.first.is_a? Character)
							word << stream.shift
						else
							break
						end
					end
					tree << word
				elsif(token.is_a? PreservedCommentStart)
					comment = PreservedComment.new
					comment << token
					while true
						peek = stream.first
						if(stream.first.is_a? MultiLineCommentEnd)
							comment << stream.shift
							break
						elsif(stream.first.nil?)
							raise UnmatchedTokenException.new(token)
						else
							comment << stream.shift
						end
					end
					tree << comment
				elsif(token.is_a? MultiLineCommentStart)
					comment = MultiLineComment.new
					comment << token
					while true
						peek = stream.first
						if(stream.first.is_a? MultiLineCommentEnd)
							comment << stream.shift
							break
						elsif(stream.first.nil?)
							raise "Unmatched multiline comment"
						else
							comment << stream.shift
						end
					end
					tree << comment
				elsif(token.is_a? SingleLineCommentStart)
					comment = SingleLineComment.new
					comment << token
					while true
						if(stream.first.is_a?(Newline) || stream.first.nil?)
							break
						else
							comment << stream.shift
						end
					end
					tree << comment
				elsif(token.is_a? QuoteCharacter)
					string = QuotedString.new
					klass = token.class
					string << token
					while true
						next_token = stream.shift
						string << next_token
						if(next_token.nil?)
							raise UnmatchedTokenException.new(token)
						elsif(next_token.is_a? klass)
							break
						end
					end
					tree << string
				elsif(token.is_a? Slash)
					regex = RegularExpression.new
					regex << token
					while true
						next_token = stream.shift
						regex << next_token
						if(next_token.nil?)
							raise UnmatchedTokenException.new(token)
						elsif(next_token.is_a? Slash)
							break
						end
					end
					tree << regex
				else
					tree << token
				end
			end
			tree
		end
		
		# Finally, do some comment-processing
		def lex_3(tree)
			i = 0
			while((current = tree[i]) != nil)
				if(current.is_a?(Comment))
					j = i - 1
					until(tree[j].nil?)
						prev = tree[j]
						if(prev.is_a? Space or prev.is_a? Tab)
							current.unshift tree.slice!(j)
						else
							break
						end
						j -= 1
						i -= 1
					end
					if(tree[i+1].is_a?(Newline))
						if(i < 1 or tree[i-1].is_a? Newline)
							current << tree.slice!(i+1)
						end
					end
				end
				i += 1
			end
			tree
		end
	end
end
