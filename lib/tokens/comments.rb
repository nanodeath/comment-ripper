class Comment < Token; end

class SingleLineCommentStart < Comment
	self.symbol = "//"
end

class MultiLineCommentStart < Comment
	self.symbol = "/*"
end

class MultiLineCommentEnd < Comment
	self.symbol = "*/"
end

class PreservedCommentStart < Comment
	self.symbol = "/*!"
end

class MultiLineComment < Comment; end

class PreservedComment < Comment; end

class SingleLineComment < Comment; end

