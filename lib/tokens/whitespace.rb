class Newline < Token
	self.symbol = "\n"
end

class Tab < Token
	self.symbol = "\t"
end

class Space < Token
	self.symbol = " "
end
