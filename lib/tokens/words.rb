class Character < Token
	def initialize(line, character, text_value)
		super(line, character)
		self.symbol = text_value
	end
end

class EscapeCharacter < Token
	self.symbol = "\\"
end

class Word < Token; end

