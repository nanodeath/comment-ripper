class QuoteCharacter < Token; end

class QuotedString < Token; end

class SingleQuote < QuoteCharacter
	self.symbol = "'"
end

class DoubleQuote < QuoteCharacter
	self.symbol = '"'
end

