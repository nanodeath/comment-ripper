class Token
	attr_reader :value
	attr_reader :subtokens
	attr_reader :position

	def initialize(line=nil, character=nil, value=nil)
		@subtokens = []
		
		p = Position.new
		p.line = line
		p.character = character
		@position = p
	end
	
	def ==(other)
		if(false && other.is_a?(Token))
			return @id == other.id
		elsif(other.is_a?(String))
			return @value == other
		else
			super
		end
	end
	
	def <<(subtoken)
		@subtokens << subtoken
	end
	
	def unshift(subtoken)
		@subtokens.unshift subtoken
	end
	
	def symbol
		@symbol || self.class.symbol
	end
	
	def symbol=(sym)
		@symbol = sym
	end
	
	class << self
		def match(other)
			other == @symbol
		end
		
		def symbol
			@symbol
		end

		protected
		def symbol=(sym)
			@symbol = sym
		end
	end
	
	def to_s
		ret = [symbol]
		ret += subtokens.map{|t| t.symbol} unless subtokens.nil?
		ret.join
	end
	
	def inspect
		ins = ["<"]
		ins << self.class.to_s
		ins << ":'"
		ins << symbol
		ins << "':["
		unless subtokens.nil? or subtokens.empty?
			ins << subtokens.map{|d| d.symbol}.join
		end
		ins << "]>"
		
		ins.join
	end
	
	class Position
		attr_accessor :line
		attr_accessor :character
	end
end
