require File.expand_path(File.dirname(__FILE__) + '/spec_helper')
require 'javascript/comment_stripper'

describe CommentRipper do
	def get_output(input)
		CommentRipper::Javascript.new.strip(input)
	end

	it "shouldn't touch non-comment code" do
		input = <<-JS
alert("cat");
		JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end

	it "should handle single-line comments on their own line" do
		input = %Q{// monkey!\nalert("cat");\n// tree!}
		
		expected_output = %q{alert("cat");} + "\n"
		output = get_output(input)

		output.should == expected_output
	end

	it "should handle multi-line comments on their own line" do
		input = %Q{/* commento */\nnewLine();}
		
		expected_output = %Q{newLine();}
		output = get_output(input)
		
		output.should == expected_output
	end

	it "should handle single-line comments at the end of regular lines" do
		input = %Q{alert("cat"); // monkey! // banana!}
		
		expected_output = %Q{alert("cat");}
		output = get_output(input)

		output.should == expected_output
	end
	
	
	it "should handle indented single-line comments correctly" do
		input = <<JS
// monkey
function(){
	// some logic
	code();
	// end logic
	bar();
}
JS
		expected_output = <<JS
function(){
	code();
	bar();
}
JS
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle indented multi-line comments correctly" do
		input = <<JS
function red(){
	/* multi-comment */
	baz();
}
JS
		expected_output = <<JS
function red(){
	baz();
}
JS
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle comments at the end of code lines" do
		input = <<JS
function(){
	end_of_the_line(); // yes, these work too.
}
JS
		expected_output = <<JS
function(){
	end_of_the_line();
}
JS
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle nested comments correctly" do
		input = <<JS
function disabled_code(){
	/*
	alert("hi");
	//*/
}

function actually_active_code(){
	//*
	alert("bye");
	//*/
}
JS
		expected_output = <<JS
function disabled_code(){
}

function actually_active_code(){
	alert("bye");
}
JS
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle single-line comments in the middle of strings" do
		input = <<JS
var red = "tilted//lines";
var green = 'single//quotes';
JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle multi-line comments in the middle of strings" do
		input = <<JS
var red = "foo /*bar*/ baz";
var green = 'foo /* bar */ baz';
JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should leave important multi-line comments intact" do
		input = <<JS
/*! Some license agreement or other! */
function coolCode() {}
JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle multi-line comments in the middle of the line" do
		input = <<JS
function poorlyNamedFunction(/* thing1 */arg1, /* thing2 */arg2){
	doNothing();
}
JS
		expected_output = <<JS
function poorlyNamedFunction(arg1,arg2){
	doNothing();
}
JS
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle comments in the middle of nefarious multiline strings" do
		input = <<JS
var animals = "chicken \
bear // kodiak \
pig // a pink one";
JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle special characters in the middle of regular expressions" do
		input = <<JS
function urlEncodeIfNecessary(s) {
    var regex = /[\\\"<>\.;]/;
    var hasBadChars = regex.exec(s) != null;
    return hasBadChars && typeof encodeURIComponent != UNDEF ? encodeURIComponent(s) : s;
}
JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end
	
	it "should handle divisions" do
		input = <<JS
        var milli = Math.floor(diff % 1000 / 100).toString();   
JS
		expected_output = input
		output = get_output(input)
		output.should == expected_output
	end
	
	describe "Unmatched symbol matching" do
		it "should give good error reporting for unmatched single quotes" do
			input = [<<JS, <<JS2, <<JS3]
var animals = ';
JS
//foo
var moo = ';
JS2
/*
 multiline
*/
function(){ var dog = ' };
JS3
			output = ["L1 C15", "L2 C11", "L4 C23"]
			(0...input.length).each do |i|
				lambda { get_output(input[i]) }.should raise_exception(CommentRipper::UnmatchedTokenException, "Unmatched token: ' at #{output[i]}")
			end
		end
		
		it "should provide good error reporting for unmatched double quotes" do 
			input = [<<JS, <<JS2, <<JS3]
var animals = ";
JS
//foo
var moo = ";
JS2
/*
 multiline
*/
function(){ var dog = " };
JS3
			output = ["L1 C15", "L2 C11", "L4 C23"]
			(0...input.length).each do |i|
				lambda { get_output(input[i]) }.should raise_exception(CommentRipper::UnmatchedTokenException, "Unmatched token: \" at #{output[i]}")
			end
		end
	end
end
